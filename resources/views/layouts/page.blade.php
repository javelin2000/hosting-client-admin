@extends('layouts.app', ['bodyId' => 'page-top'])

@section('content')

    <div id="wrapper">
        @include('layouts.navbar')

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('layouts.topbar')

                <div class="container-fluid">
                    @yield('page-content')
                </div>

            </div>

            @include('layouts.footer')

        </div>
    </div>

    @include('layouts.scroll-to-top')
    @include('layouts.logout-modal')

@endsection
