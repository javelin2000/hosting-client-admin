@extends('layouts.page')

@section('page-content')
    <h1 class="h3 mb-4 text-gray-800">Profile</h1>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xl-2 col-md-4 col-md-4 col-sm-12">
            <div class="main-avatar-div">
                <img
                    class="img-main-avatar rounded-circle"
                    id="img-main-avatar"
                    alt="Click to load new avatar"
                    src="{{ $user->avatar_url }}"
                />
            </div>
            <form id="avatar-form" method="POST" action="{{ route('user.avatar', $user->id) }}" enctype="multipart/form-data">
                <div>
                    <input type="file" name="avatar" accept="image/*" style="display: none" id="selectFileDialog">
                </div>
                <div>
                    <button id="uploadnow" style="display: none" class="btn btn-primary small">
                        Upload now!
                    </button>
                </div>
                <div class="ajax-respond"></div>

                @error('avatar')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </form>

        </div>

        <div class="col-md-6 col-sm-12">
            <form class="user" method="POST" action="{{ route('user.update', $user->id) }}">
                @csrf

                <input type="hidden" name="_method" value="PUT">

                <div class="form-group">
                    <input
                        id="name"
                        type="text"
                        class="form-control form-control-user @error('name') is-invalid @enderror"
                        placeholder="Name"
                        name="name"
                        value="{{ old('name', $user->name) }}"
                        autocomplete="name"
                        autofocus
                    >

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input
                        id="email"
                        type="email"
                        class="form-control form-control-user @error('email') is-invalid @enderror"
                        placeholder="Email Address"
                        name="email"
                        value="{{ old('email', $user->email) }}"
                        autocomplete="email"
                    >

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input
                            id="password"
                            type="password"
                            class="form-control form-control-user @error('password') is-invalid @enderror"
                            placeholder="New Password"
                            name="password"
                            autocomplete="new-password"
                        >

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="col-sm-6">
                        <input
                            id="password-confirm"
                            type="password"
                            class="form-control form-control-user"
                            name="password_confirmation"
                            placeholder="Repeat New Password"
                            autocomplete="new-password"
                        >
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <input
                        id="country"
                        type="text"
                        class="form-control form-control-user @error('address.country') is-invalid @enderror"
                        placeholder="Country"
                        name="address[country]"
                        value="{{ old('address.country', $user->mainAddress ? $user->mainAddress->country : null) }}"
                        autocomplete="country"
                        autofocus
                    >

                    @error('address.country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input
                        id="city"
                        type="text"
                        class="form-control form-control-user @error('address.city') is-invalid @enderror"
                        placeholder="City"
                        name="address[city]"
                        value="{{ old('address.city', $user->mainAddress ? $user->mainAddress->city : null) }}"
                        autocomplete="city"
                        autofocus
                    >

                    @error('address.city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input
                        id="street"
                        type="text"
                        class="form-control form-control-user @error('address.street') is-invalid @enderror"
                        placeholder="Street"
                        name="address[street]"
                        value="{{ old('address.street', $user->mainAddress ? $user->mainAddress->street : null) }}"
                        autocomplete="street"
                        autofocus
                    >

                    @error('address.street')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input
                        id="zip_code"
                        type="text"
                        class="form-control form-control-user @error('address.zip_code') is-invalid @enderror"
                        placeholder="Zip code"
                        name="address[zip_code]"
                        value="{{ old('address.zip_code', $user->mainAddress ? $user->mainAddress->zip_code : null) }}"
                        autocomplete="zip_code"
                        autofocus
                    >

                    @error('address.zip_code')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Save
                </button>
            </form>
        </div>
    </div>

@endsection
