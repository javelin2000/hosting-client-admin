@extends('layouts.page')

@section('page-content')
    <h1 class="h3 mb-4 text-gray-800">Domain List</h1>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Plan</th>
                            <th>Status</th>
                            <th>Hosting features</th>
                            <th>Type</th>
                            <th>Start date</th>
                            <th>Date of Expiry</th>
                            <th>Price</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($hostings as $hosting)
                            <tr>
                                <td>{{ $hosting->pricingPlan->name }}</td>
                                <td>{{ $hosting->is_active }}</td>
                                <td>
                                    <ul>
                                        <li class="list1">{{ $hosting->pricingPlan->disk_space }}GB SSD Space</li>
                                        <li class="list2">{{ $hosting->pricingPlan->memory }}MB Memory</li>
                                        <li class="list3">{{ $hosting->pricingPlan->vcpu_core_count }} Core vCPU</li>
                                        <li class="list4">{{ $hosting->pricingPlan->bandwidth }} TB/mo Bandwidth</li>
                                        <li class="list5">{{ $hosting->pricingPlan->mysql }}</li>
                                        <li class="list6">{{ $hosting->pricingPlan->emails_count }} E-mails</li>
                                        <li class="list7">{{ $hosting->pricingPlan->ftp_accounts_count }} FTP Accounts</li>
                                        <li class="list8">{{ $hosting->pricingPlan->support }}</li>
                                    </ul>
                                </td>
                                <td>{{ $hosting->type->title }}</td>
                                <td>{{ $hosting->start_date }}</td>
                                <td>{{ $hosting->expiry_date }}</td>
                                <td>{{ $hosting->total_cost }}</td>
                                <td>
                                    <a href="#" class="btn btn-warning btn-circle btn-sm">
                                        <i class="fas fa-cog"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
