@extends('layouts.page')

@section('page-content')
    <h1 class="h3 mb-4 text-gray-800">Domain List</h1>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Domain</th>
                            <th>Status</th>
                            <th>Auto-Renew</th>
                            <th>Start date</th>
                            <th>Expiration</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($domains as $domain)
                            <tr>
                                <td>{{ $domain->domain }}</td>
                                <td>{{ $domain->is_active }}</td>
                                <td>{{ $domain->auto_renew }}</td>
                                <td>{{ \Carbon\Carbon::parse($domain->created)->format('d/m/Y H:i') }}</td>
                                <td>{{ \Carbon\Carbon::parse($domain->expires)->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="#" class="btn btn-warning btn-circle btn-sm">
                                        <i class="fas fa-cog"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
