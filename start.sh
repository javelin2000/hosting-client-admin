#!/bin/bash
# update project from repo
git fetch
git pull

# Put the application into maintenance mode
php artisan down

[ ! -x "$(command -v composer)" ] && curl -sS https://getcomposer.org/installer | php

#Install project dependencies
composer install

if [ -n "$1" ] && [ $1 = "install" ]; then
    if [ ! -f ./.env ]; then
            cp .env.example .env
    fi

    php artisan key:generate
    php artisan jwt:secret
fi

#Clear Cache
php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan optimize:clear
php artisan config:cache

# Run database migrations and seeds.
if [ -n "$1" ] && [ $1 = "fresh" ]; then
    php artisan migrate:fresh --seed
else
    php artisan migrate --seed
fi

# Bring the application out of maintenance mode
php artisan up

echo "Complete"