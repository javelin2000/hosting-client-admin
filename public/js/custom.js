$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#img-main-avatar').click(function (e) {
        e.preventDefault();
        $('#selectFileDialog').trigger("click");

        $(':file').change(function () {
            console.log(this.files, this.files[0]);
            if (this.files[0].size > 0) {
                $('#uploadnow').click();
                return false;
            }
        });
    });

    $("form#avatar-form").submit(function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: new FormData(this),
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (loadResult, textStatus) {
                if (!loadResult.errors) {
                    document.images["img-main-avatar"].src = loadResult.url;
                } else {
                    alert('Error:' + loadResult.errors)
                }
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('Avatar doesn\'t uploaded! Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Avatar doesn\'t uploaded! Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Avatar doesn\'t uploaded! Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Avatar doesn\'t uploaded! Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Avatar doesn\'t uploaded! Time out error.');
                } else if (exception === 'abort') {
                    alert('Avatar doesn\'t uploaded! Ajax request aborted.');
                } else {
                    alert('Avatar doesn\'t uploaded! Uncaught Error.\n' + $.parseJSON(jqXHR.responseText).errors);
                }
            }
        });

        return false;
    });
});
