<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@Callback');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::post('logout-and-delete', 'Auth\LoginController@logoutAndDelete')->name('logout-and-delete');

    Route::get('/', 'IndexController@index')->name('index');

    Route::get('profile', 'UserController@profile')->name('profile');
    Route::put('user/{user}', 'UserController@update')->name('user.update');
    Route::post('avatar/{user}', 'UserController@saveAvatar')->name('user.avatar');

    Route::get('domains', 'DomainController@index')->name('domains');
    Route::get('hostings', 'HostingController@index')->name('hostings');

    Route::get('buttons', 'IndexController@index')->name('buttons');
    Route::get('cards', 'IndexController@index')->name('cards');
    Route::get('utilities-color', 'IndexController@index')->name('utilities-color');
    Route::get('utilities-border', 'IndexController@index')->name('utilities-border');
    Route::get('utilities-animation', 'IndexController@index')->name('utilities-animation');
    Route::get('utilities-other', 'IndexController@index')->name('utilities-other');
    Route::get('charts', 'IndexController@index')->name('charts');
    Route::get('tables', 'IndexController@index')->name('tables');
});
