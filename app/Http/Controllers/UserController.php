<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\Address;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @return Factory|View
     */
    public function profile()
    {
        return view('profile')->with([
            'user' => Auth::user()
        ]);
    }

    /**
     * @param UserUpdateRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        try {
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password ? Hash::make($request->password) : $user->password
            ]);

            Address::updateOrCreate(
                ['user_id' => $user->id, 'is_main' => 1],
                $request->address
            );

            $request->session()->flash('status', 'Data has been updated successfully.');

            return redirect()->route('profile');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            abort(500);
        }
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function saveAvatar(Request $request, User $user)
    {
        try {
            Validator::make(
                $request->all(),
                ['avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048']
            )->validate();

            $avatarName = $user->id . '_avatar' . time() . '.' . $request->avatar->getClientOriginalExtension();
            $user->update([
                'avatar' => Storage::url($request->avatar->storeAs('avatars', $avatarName, 'public')),
            ]);

            return response()->json([
                'success' => true,
                'url' => $user->avatar
            ], 200);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json([
                'success' => false,
                'errors'  => $e->getMessage(),
            ], 400);
        }
    }
}
