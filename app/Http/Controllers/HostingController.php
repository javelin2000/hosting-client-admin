<?php

namespace App\Http\Controllers;

use App\Models\UserDomain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HostingController extends Controller
{
    public function index()
    {
        return view('hosting.index')->with([
            'hostings' => Auth::user()->userHostings
        ]);
    }
}
