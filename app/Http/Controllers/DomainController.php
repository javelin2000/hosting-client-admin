<?php

namespace App\Http\Controllers;

use App\Models\UserDomain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DomainController extends Controller
{
    public function index()
    {
        return view('domain.index')->with([
            'domains' => Auth::user()->userDomains
        ]);
    }
}
