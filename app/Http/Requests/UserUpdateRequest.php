<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore(Auth::id())->whereNull('deleted_at')],
            'password' => 'nullable|string|min:8|confirmed',
            'address' => 'array',
            'address.country' => 'nullable|required_with:address.city,address.street|string|max:255',
            'address.city' => 'nullable|required_with:address.street|string|max:255',
            'address.street' => 'nullable|string|max:255',
            'address.zip_code' => 'nullable|string|max:255',
        ];
    }
}
