<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id',
        'is_main',
        'country',
        'city',
        'street',
        'zip_code',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
