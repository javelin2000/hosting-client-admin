<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PricingPlan;
use App\Models\HostingFeature;
use App\Models\Page;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Hosting extends Model
{
    protected $fillable = [
        'name',
        'title',
        'menu_img_path',
        'description',
        'is_active',
        'icon',
        'section_title',
        'section_description',
        'section_img'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return HasMany
     */
    public function pricingPlans()
    {
        return $this->hasMany(HostingPricingPlan::class, 'hosting_id');
    }

    /**
     * @return BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany(HostingFeature::class, 'hosting_hosting_feature', 'hosting_id', 'feature_id');
    }
}
