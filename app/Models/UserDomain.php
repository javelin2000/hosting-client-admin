<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDomain extends Model
{
    protected $fillable = [
        'user_id',
        'domain',
        'is_active',
        'created',
        'expires',
        'auto_renew',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
