<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingPlanType extends Model
{
    const TYPE_MONTHLY = 'monthly';
    const TYPE_QUARTERLY = 'quarterly';
    const TYPE_HALF_YEARLY = 'half_yearly';
    const TYPE_YEARLY = 'yearly';

    protected $fillable = [
        'is_active',
        'name',
        'title',
        'duration',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
