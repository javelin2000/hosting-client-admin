<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostingFeature extends Model
{
    protected $fillable = [
        'icon',
        'title',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
