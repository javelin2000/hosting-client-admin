<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserHosting extends Model
{
    protected $fillable = [
        'user_id',
        'plan_id',
        'type_id',
        'start_date',
        'total_cost',
        'is_active',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'start_date'
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function pricingPlan()
    {
        return $this->belongsTo(HostingPricingPlan::class, 'plan_id');
    }

    /**
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(PricingPlanType::class, 'type_id');
    }

    public function setTotalCostAttribute()
    {
        $planDiscount = $this->pricingPlan->discount->{$this->type->name} ?? 0;
        $totalDiscount = $planDiscount + $this->user->discount;
        $fullPrice = $this->pricingPlan->monthly_price * $this->type->duration;
        $totalCost =  $fullPrice - $fullPrice * $totalDiscount * 0.01;

        $this->attributes['total_cost'] = $totalCost;
    }

    /**
     * @return Carbon
     */
    public function getExpiryDateAttribute()
    {
        return Carbon::parse($this->start_date)->addMonth($this->type->duration);
    }
}
