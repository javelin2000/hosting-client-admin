<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class HostingPricingPlan extends Model
{
    protected $fillable = [
        'name',
        'hosting_id',
        'is_active',
        'disk_space',
        'memory',
        'vcpu_core_count',
        'bandwidth',
        'mysql',
        'emails_count',
        'ftp_accounts_count',
        'support',
        'monthly_price'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return BelongsTo
     */
    public function hosting()
    {
        return $this->belongsTo(Hosting::class);
    }

    /**
     * @return HasOne
     */
    public function discount()
    {
        return $this->hasOne(Discount::class, 'plan_id');
    }
}
