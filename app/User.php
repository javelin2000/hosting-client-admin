<?php

namespace App;

use App\Models\Address;
use App\Models\UserDomain;
use App\Models\UserHosting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'provider',
        'provider_id',
        'email_verified_at',
        'discount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return mixed|string
     */
    public function getAvatarUrlAttribute()
    {
        return $this->avatar ?: asset('img/default-avatar.jpg');
    }

    /**
     * @return HasMany
     */
    public function userDomains()
    {
        return $this->hasMany(UserDomain::class);
    }

    /**
     * @return HasMany
     */
    public function userHostings()
    {
        return $this->hasMany(UserHosting::class);
    }

    /**
     * @return HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * @return Model|HasMany|object|null
     */
    public function getMainAddressAttribute()
    {
        return $this->addresses()->where('is_main', 1)->first();
    }
}
